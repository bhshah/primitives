#!/usr/bin/env python3

# A simple script to help add a new (or updated) primitive annotation. Provide it with a list
# of paths to primitive annotations, and it will copy them into the place inside the repository.

import json
import os
import os.path
import re
import sys


json_decoder = json.JSONDecoder()

WHITESPACE = re.compile(r'[ \t\n\r]*', re.MULTILINE)


for primitive_annotation_path in sys.argv[1:]:
    primitive_annotations = []
    print("Reading {}".format(primitive_annotation_path))
    with open(primitive_annotation_path, 'r') as primitive_annotation_file:
        content = primitive_annotation_file.read()
        pos = 0
        while pos < len(content):
            primitive_annotation, pos = json_decoder.raw_decode(content, pos)
            primitive_annotations.append(primitive_annotation)
            pos = WHITESPACE.match(content, pos).end()
            print("    {}".format(primitive_annotation['python_path']))

    for primitive_annotation in primitive_annotations:
        target_path = os.path.join(
            'primitives',
            primitive_annotation['source']['name'],
            primitive_annotation['python_path'],
            primitive_annotation['version'],
            'primitive.json',
        )
        with open(target_path, 'w') as f_out:
            json.dump(primitive_annotation, f_out, indent=4)
            f_out.write('\n')
