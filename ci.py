#!/usr/bin/env python3

import argparse
import copy
import datetime
import glob
import itertools
import json
import logging
import os
import os.path
import subprocess
import sys
import time
import uuid
import yaml
from logging import config
from urllib import parse as url_parse

try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader

import docker
import docker.types

try:
    from pip.utils import logging as logging_utils
except ModuleNotFoundError:
    from pip._internal.utils import logging as logging_utils

TIMESTAMPED_FORMAT = '[%(asctime)s] %(message)s'
SIMPLE_FORMAT = '%(message)s'
MAIN_REPOSITORY = 'https://gitlab.com/datadrivendiscovery/primitives.git'

logger = logging.getLogger(__name__)

docker_client = docker.from_env(version='auto')


class ColorizedStreamHandler(logging_utils.ColorizedStreamHandler):
    if logging_utils.colorama:
        COLORS = logging_utils.ColorizedStreamHandler.COLORS + [
            (logging.INFO, logging_utils._color_wrap(logging_utils.colorama.Style.BRIGHT)),
        ]
    else:
        COLORS = []


class JsonEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.astimezone(datetime.timezone.utc).replace(tzinfo=None).isoformat('T') + 'Z'

        return super().default(o)


def get_duration(start_at, end_at):
    return (end_at - start_at).total_seconds()

def private_git_uri_access(uri):
    """
    Convert a git URI to something which can be accesses.
    """

    parsed_uri = url_parse.urlparse(uri)

    if parsed_uri.hostname != 'gitlab.datadrivendiscovery.org':
        # Not a private URI.
        return uri

    # Remove anything before "@", if it exists.
    _, _, netloc = parsed_uri.netloc.rpartition('@')

    if 'CI_JOB_TOKEN' in os.environ:
        # If we are running on GitLab CI.
        parsed_uri = parsed_uri._replace(netloc='gitlab-ci-token:{CI_JOB_TOKEN}@{netloc}'.format(
            CI_JOB_TOKEN=os.environ['CI_JOB_TOKEN'],
            netloc=netloc,
        ))
        parsed_uri = parsed_uri._replace(scheme='https')
    elif arguments.username and arguments.password:
        # If username and password were provided.
        parsed_uri = parsed_uri._replace(netloc='{username}:{password}@{netloc}'.format(
            username=arguments.username,
            password=arguments.password,
            netloc=netloc,
        ))
        parsed_uri = parsed_uri._replace(scheme='https')
    else:
        # We are running somewhere else. Assume that SSH git URIs will work.
        parsed_uri = parsed_uri._replace(netloc='git@{netloc}'.format(
            netloc=netloc,
        ))
        parsed_uri = parsed_uri._replace(scheme='ssh')

    return url_parse.urlunparse(parsed_uri)


# TODO: Configure run timeout to an hour or so.
#       See: https://gitlab.datadrivendiscovery.org/d3m/pipelines-ci/issues/1
def run_pipeline_run(docker_image, docker_image_digest, docker_volumes, output_dir, pipeline_run, gpus):
    results = copy.copy(pipeline_run)
    success = None

    # This is not a standard pipeline run.
    ci_run_id = str(uuid.uuid4())
    docker_container = None

    results['ci_run_id'] = ci_run_id

    logger.info(">>> [%(ci_run_id)s] Re-running pipeline run '%(pipeline_run_path)s'.", {'pipeline_run_path': pipeline_run['pipeline_run_path'], 'ci_run_id': ci_run_id})

    results['start_at'] = datetime.datetime.now(datetime.timezone.utc)

    try:
        os.makedirs(os.path.join(output_dir, ci_run_id), 0o755, exist_ok=False)

        args = [
            'python3', '-m', 'd3m',
            '--pipelines-path', f'''/primitives/{pipeline_run['pipelines_dir']}''',
            'runtime',
            '--datasets', '/datasets',
            '--volumes', '/volumes',
            'fit-score',
            '--input-run', f'''/primitives/{pipeline_run['pipeline_run_path_decompressed']}''',
            '--output', f'/outputs/{ci_run_id}/predictions.csv',
            '--scores', f'/outputs/{ci_run_id}/scores.csv',
            '--output-run', f'/outputs/{ci_run_id}/pipeline_runs.yaml',
            '--expose-produced-outputs', f'/outputs/{ci_run_id}/outputs',
        ]

        with open(os.path.join(output_dir, ci_run_id, 'run.log'), 'xb') as log_file:
            if gpus:
                kwargs = {
                    'device_requests': [
                        docker.types.DeviceRequest(device_ids=gpus, capabilities=[['gpu']]),
                    ],
                }
            else:
                kwargs = {}

            docker_container = docker_client.containers.create(
                docker_image,
                args,
                environment={
                    'D3M_BASE_IMAGE_NAME': docker_image,
                    'D3M_BASE_IMAGE_DIGEST': docker_image_digest,
                    'D3M_IMAGE_NAME': docker_image,
                    'D3M_IMAGE_DIGEST': docker_image_digest,
                },
                detach=True, auto_remove=True,
                stop_signal='SIGKILL', volumes=docker_volumes,
                # TODO: For pipelines containing non-pure primitives, allow Internet access.
                #       See: https://gitlab.datadrivendiscovery.org/d3m/pipelines-ci/issues/4
                network_disabled=True,
                **kwargs,
            )

            docker_repo_digest = f'{docker_image.split(":")[0]}@{docker_image_digest}'
            assert docker_image in docker_container.image.tags, (docker_image, docker_container.image.tags)
            assert docker_repo_digest in docker_container.image.attrs['RepoDigests'], (docker_repo_digest, docker_container.image.attrs['RepoDigests'])

            results['image'] = {
                'id': docker_container.image.id,
                'labels': docker_container.image.labels,
                'tags': docker_container.image.tags,
                'repo_digests': docker_container.image.attrs['RepoDigests'],
            }

            docker_container.start()

            for line in docker_container.logs(stream=True):
                log_file.write(line)
                logger.debug(line.decode('utf8').rstrip())

            exit_status = docker_container.wait(timeout=60)
            docker_container = None

        if exit_status:
            logger.error(">>> ERROR [%(ci_run_id)s] Run failed.", {'ci_run_id': ci_run_id})
            success = False
        else:
            logger.info(">>> SUCCESS [%(ci_run_id)s] Run succeeded.", {'ci_run_id': ci_run_id})
            success = True

        results['end_at'] = datetime.datetime.now(datetime.timezone.utc)
        results['duration'] = get_duration(results['start_at'], results['end_at'])

    except Exception:
        logger.exception(">>> ERROR [%(ci_run_id)s] Run failed.", {'ci_run_id': ci_run_id})

        results['end_at'] = datetime.datetime.now(datetime.timezone.utc)
        results['duration'] = get_duration(results['start_at'], results['end_at'])

    finally:
        if docker_container is not None:
            try:
                docker_container.stop()
            except Exception:
                logger.exception(">>> ERROR Error stopping Docker container.")

    results['success'] = success

    return results, not success


def run_pipeline_runs_for_image_type(image_tag, image_type, pipeline_runs, *, primitives_dirs, datasets_dir, output_dir, volumes_dir=None, gpus=None):
    results = []
    has_error = False

    try:
        docker_image = f'registry.gitlab.com/datadrivendiscovery/images/primitives:{image_tag}-{image_type}'
        docker_image_digest = None

        # First we pull the latest version of the image.
        logger.info("Pulling Docker image '%(docker_image)s'.", {'docker_image': docker_image})
        previous_chunk = None
        for chunk in docker_client.api.pull(docker_image, stream=True, decode=True):
            if 'status' in chunk:
                if chunk['status'].startswith('Digest: sha256:'):
                    docker_image_digest = chunk['status'][len('Digest: '):]

                if previous_chunk != chunk['status']:
                    logger.debug(chunk['status'])

                previous_chunk = chunk['status']

        # For some reason running the first container immediately after pulling often
        # ends up with timeout talking to the Docker daemon. We wait a bit as a workaround.
        logger.info("Waiting for a bit after pulling the Docker image.")
        time.sleep(30)

        docker_volumes = []

        if datasets_dir:
            docker_volumes.append(f'{datasets_dir}:/datasets:ro')
        else:
            docker_volumes.append('/datasets')

        if volumes_dir:
            docker_volumes.append(f'{volumes_dir}:/volumes:ro')
        else:
            docker_volumes.append('/volumes')

        # At least one directory had to be provided, unless we would not be here.
        assert primitives_dirs
        for primitives_dir in primitives_dirs:
            docker_volumes.append(f'{os.path.abspath(primitives_dir)}:/primitives/{primitives_dir}:ro')

        # Output directory is required.
        assert output_dir
        docker_volumes.append(f'{os.path.abspath(output_dir)}:/outputs:rw')

        processed_pipeline_runs = set()
        for pipeline_run in pipeline_runs:
            for pipeline_run_id in pipeline_run['pipeline_run_ids']:
                if pipeline_run_id in processed_pipeline_runs:
                    logger.info("Pipeline run '%(pipeline_run_path)s' is a duplicate and has already been re-run. Skipping.", {'pipeline_run_path': pipeline_run['pipeline_run_path']})
                else:
                    processed_pipeline_runs.add(pipeline_run_id)

            pipeline_run_result, error = run_pipeline_run(docker_image, docker_image_digest, docker_volumes, output_dir, pipeline_run, gpus)
            results.append(pipeline_run_result)
            if error:
                has_error = True

    except Exception:
        logger.exception(">>> ERROR Unexpected exception.")
        has_error = True

    return results, has_error


def get_all_primitives(primitives_dirs, limit_prefix, files_changed):
    primitives = []
    error = False

    for primitives_directory in primitives_dirs:
        primitives_directory_limit_prefix = f'{primitives_directory}/{limit_prefix}'

        for dirpath, dirnames, filenames in os.walk(primitives_directory):
            if dirpath.startswith(primitives_directory_limit_prefix) and 'primitive.json' in filenames:
                primitive_path = os.path.join(dirpath, 'primitive.json')

                # if were are only to run pipelines CI on modified files, then files_changed should
                # be non-empty and the primitive's exact path should be in files_changed
                if files_changed and primitive_path not in files_changed:
                    continue

                try:
                    with open(primitive_path, 'r', encoding='utf8') as primitive_file:
                        primitive = json.load(primitive_file)

                    primitives.append({
                        'primitive_path': primitive_path,
                        'id': primitive['id'],
                        'version': primitive['version'],
                        'name': primitive['name'],
                        'python_path': primitive['python_path'],
                        'digest': primitive.get('digest', None),
                    })
                except Exception:
                    logger.exception(">>> ERROR Error processing primitive '%(primitive_path)s'.", {'primitive_path': primitive_path})
                    error = True

    if limit_prefix:
        logger.info(
            "Found %(primitives_count)s primitive(s) under %(limit_prefix)s.",
            {
                'primitives_count': len(primitives),
                'limit_prefix': limit_prefix,
            },
        )
    else:
        logger.info(
            "Found %(primitives_count)s primitive(s).",
            {
                'primitives_count': len(primitives),
            },
        )

    for primitive in primitives:
        logger.debug("%(primitive_path)s", {'primitive_path': primitive['primitive_path']})

    return primitives, error


def get_all_pipeline_runs(primitives_dirs, limit_prefix, files_changed):
    pipeline_runs = []
    error = False

    for primitives_directory in primitives_dirs:
        primitives_directory_limit_prefix = f'{primitives_directory}/{limit_prefix}'

        for dirpath, dirnames, filenames in os.walk(primitives_directory):
            if dirpath.startswith(primitives_directory_limit_prefix) and 'pipeline_runs' in dirnames:
                # Do not traverse further.
                dirnames[:] = []

                pipeline_runs_dir = os.path.join(dirpath, 'pipeline_runs')
                pipelines_dir = os.path.join(dirpath, 'pipelines')

                if not os.path.exists(pipelines_dir):
                    logger.error(">>> ERROR Pipelines for pipeline runs '%(pipeline_runs_dir)s' cannot be found.", {'pipeline_runs_dir': pipeline_runs_dir})
                    error = True
                    continue

                for pipeline_run_path in itertools.chain(
                    glob.iglob(f'{pipeline_runs_dir}/*.yaml'),
                    glob.iglob(f'{pipeline_runs_dir}/*.yml'),
                    glob.iglob(f'{pipeline_runs_dir}/*.yaml.gz'),
                    glob.iglob(f'{pipeline_runs_dir}/*.yml.gz'),
                ):
                    try:
                        if files_changed and pipeline_run_path not in files_changed:
                            continue
                        elif pipeline_run_path.endswith('.gz'):
                            logger.debug("Decompressing '%(pipeline_run_path)s'.", {'pipeline_run_path': pipeline_run_path})
                            # Decompressing.
                            subprocess.run(['gzip', '-d', '-f', pipeline_run_path], check=True)

                            # Removing ".gz" suffix.
                            pipeline_run_path_decompressed = pipeline_run_path[:-3]
                        else:
                            pipeline_run_path_decompressed = pipeline_run_path

                        logger.debug("Found '%(pipeline_run_path)s' pipeline run.", {'pipeline_run_path': pipeline_run_path})

                        pipeline_run_ids = []
                        with open(pipeline_run_path_decompressed, 'r', encoding='utf8') as pipeline_run_file:
                            for pipeline_run in yaml.load_all(pipeline_run_file, Loader=SafeLoader):
                                pipeline_run_ids.append(pipeline_run['id'])

                        pipeline_runs.append({
                            'pipeline_run_path': pipeline_run_path,
                            'pipeline_run_path_decompressed': pipeline_run_path_decompressed,
                            'pipelines_dir': pipelines_dir,
                            'pipeline_run_ids': pipeline_run_ids,
                        })

                    except Exception:
                        logger.exception(">>> ERROR Error processing pipeline run '%(pipeline_run_path)s'.", {'pipeline_run_path': pipeline_run_path})
                        error = True

    if limit_prefix:
        logger.info(
            "Found %(pipeline_runs_count)s pipeline run(s) under %(limit_prefix)s.",
            {
                'pipeline_runs_count': len(pipeline_runs),
                'limit_prefix': limit_prefix,
            },
        )
    else:
        logger.info(
            "Found %(pipeline_runs_count)s pipeline run(s).",
            {
                'pipeline_runs_count': len(pipeline_runs),
            },
        )

    for pipeline_run in pipeline_runs:
        logger.debug("%(pipeline_run_path)s", {'pipeline_run_path': pipeline_run['pipeline_run_path']})

    return pipeline_runs, error


def get_gpu_uuid(enabled_gpu, all_gpus):
    for index, uuid, gpu_bus_id in all_gpus:
        if gpu_bus_id.endswith(enabled_gpu):
            return uuid
    else:
        raise KeyError(f"Could not find GPU UUID for '{enabled_gpu}'.")


def main():
    results = {}
    has_errored = False

    # To have both stderr and stdout interleaved together.
    sys.stderr = sys.stdout

    config.dictConfig(
        {
            'version': 1,
            'disable_existing_loggers': True,
            'formatters': {
                'timestamped': {
                    'format': TIMESTAMPED_FORMAT,
                },
                'simple': {
                    'format': SIMPLE_FORMAT,
                },
            },
            'handlers': {
                'console': {
                    'class': '__main__.ColorizedStreamHandler',
                    'formatter': 'timestamped',
                    'level': 'DEBUG',
                },
            },
            'loggers': {
                '__main__': {
                    'level': 'DEBUG',
                    'handlers': [
                        'console',
                    ],
                },
            },
        },
    )

    logging.captureWarnings(True)

    parser = argparse.ArgumentParser(description="Test pipelines.")
    parser.add_argument(
        '-d', '--datasets', action='store', dest='datasets_dir', required=True,
        help="path to a directory with datasets (and problem descriptions) to resolve IDs in pipeline run files",
    )
    parser.add_argument(
        '-o', '--output', action='store', dest='output_dir', required=True,
        help="path to an output directory to store results in",
    )
    parser.add_argument(
        '-v', '--volumes', action='store', dest='volumes_dir',
        help="path to a directory with static files required by primitives, in the standard directory structure (as obtained running \"python3 -m d3m primitive download\")",
    )
    parser.add_argument(
        '-l', '--limit', action='store', default=os.environ.get('D3M_LIMIT_PREFIX', ''), dest='limit_prefix',
        help="limit only to files with this path prefix, has priority over D3M_LIMIT_PREFIX environment variable",
    )
    parser.add_argument(
        '-i', '--image', action='store', default="ubuntu-bionic-python36", dest='image_tag',
        help="which image tag to use, default is \"ubuntu-bionic-python36\"",
    )
    parser.add_argument(
        '-t', '--type', action='store', default=os.environ.get('D3M_IMAGE_TYPE', 'master'), dest='image_type',
        help="which image type to use, has priority over D3M_IMAGE_TYPE environment variable, default is \"master\"",
    )
    parser.add_argument(
        '-g', '--gpu', action='store_true', default=False, dest='use_gpu',
        help="enable GPU support",
    )
    parser.add_argument(
        '-m', '--modified_files_only', action='store_true', default=False, dest='modified_files_only',
        help="only run pipelines CI for modified primitives and pipeline runs",
    )
    parser.add_argument('primitives_dirs', metavar='DIR', nargs='*', help="directory with primitive annotations")
    arguments = parser.parse_args()

    if arguments.use_gpu:
        result = subprocess.run(['nvidia-smi', '--query-gpu=index,uuid,gpu_bus_id', '--format=csv,noheader,nounits'], stdout=subprocess.PIPE, encoding='utf8', check=True)
        all_gpus = [line.split(', ') for line in result.stdout.splitlines()]
        all_enabled_gpus = os.listdir('/proc/driver/nvidia/gpus/')
        gpus = [get_gpu_uuid(enabled_gpu, all_gpus) for enabled_gpu in all_enabled_gpus]
    else:
        gpus = None

    seen_primitives_dirs = set()
    for primitives_directory in arguments.primitives_dirs:
        if not os.path.exists(primitives_directory):
            raise ValueError(f"Required primitives directory \"{primitives_directory}\" does not exist.")

        if os.path.normpath(primitives_directory) != primitives_directory:
            raise ValueError(f"Primitives directory \"{primitives_directory}\" path is not normalized.")

        real_primitives_directory = os.path.realpath(primitives_directory)
        if real_primitives_directory != os.path.abspath(primitives_directory):
            raise ValueError(f"Primitives directory \"{primitives_directory}\" path contains symlinks.")

        normalized_primitives_directory = os.path.normpath(primitives_directory)
        path_segments = normalized_primitives_directory.split(os.path.sep)
        for invalid_path_segment in ['..', '.']:
            if invalid_path_segment in path_segments:
                raise ValueError(f"Primitives directory \"{primitives_directory}\" path contains \"{invalid_path_segment}\".")

        if normalized_primitives_directory.startswith('/'):
            raise ValueError(f"Primitives directory \"{primitives_directory}\" path starts with \"/\".")

        if real_primitives_directory in seen_primitives_dirs:
            raise ValueError(f"Primitives directory \"{primitives_directory}\" has already been listed.")
        seen_primitives_dirs.add(real_primitives_directory)

    os.makedirs(arguments.output_dir, 0o755, exist_ok=True)

    files_changed = []

    if arguments.modified_files_only:
        main_repository = private_git_uri_access(MAIN_REPOSITORY)

        # We allow it to fail.
        subprocess.run([
            'git', 'remote', 'remove', 'upstream',
        ], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, encoding='utf8')

        subprocess.run([
            'git', 'remote', 'add', '-f', 'upstream', main_repository,
        ], stdout=sys.stdout, stderr=sys.stderr, check=True, encoding='utf8')

        subprocess.run([
            'git', 'remote', 'update',
        ], stdout=sys.stdout, stderr=sys.stderr, check=True, encoding='utf8')

        changes = subprocess.run([
            'git', 'diff', '--name-status', '--no-renames', 'remotes/upstream/master', 'HEAD',
        ], stdout=subprocess.PIPE, stderr=sys.stderr, check=True, encoding='utf8').stdout.splitlines()

        for change in changes:
            change_type, _, file_changed = change.partition('\t')

            # We skip deletions of primitive annotations. But only those deletions.
            if change_type == 'D' and file_changed.startswith('primitives/'):
                continue

            files_changed.append(file_changed)

    all_primitives, error = get_all_primitives(arguments.primitives_dirs, arguments.limit_prefix, files_changed)
    if error:
        has_errored = True

    all_pipeline_runs, error = get_all_pipeline_runs(arguments.primitives_dirs, arguments.limit_prefix, files_changed)
    if error:
        has_errored = True

    results = {
        'primitives': all_primitives,
        'pipeline_runs': all_pipeline_runs,
        'pipeline_run_results': {},
    }

    results['pipeline_run_results'][arguments.image_type], error = run_pipeline_runs_for_image_type(
        arguments.image_tag,
        arguments.image_type,
        all_pipeline_runs,
        primitives_dirs=arguments.primitives_dirs,
        datasets_dir=arguments.datasets_dir,
        output_dir=arguments.output_dir,
        volumes_dir=arguments.volumes_dir,
        gpus=gpus,
    )
    if error:
        has_errored = True

    results['success'] = not has_errored

    results['config'] = {
        'limit_prefix': arguments.limit_prefix,
        'user': os.environ.get('D3M_USER', None) or None,
    }

    with open(os.path.join(arguments.output_dir, 'results.json'), 'w') as results_file:
        json.dump(results, results_file, indent=4, cls=JsonEncoder)

    logger.info(">>> Results:")
    error_count = 0
    for interface_version, pipeline_run_results in results['pipeline_run_results'].items():
        for pipeline_run_result in pipeline_run_results:
            if not pipeline_run_result['success']:
                error_count += 1
            logger.info('%(pipeline_run_path)s: %(result)s', {'pipeline_run_path': pipeline_run_result['pipeline_run_path'], 'result': 'success' if pipeline_run_result['success'] else 'ci failed' if pipeline_run_result['success'] is None else 'pipeline run failed'})

    if has_errored:
        if error_count:
            logger.error(">>> There were %(error_count)s error(s).", {'error_count': error_count})
        else:
            logger.error(">>> There were error(s).")
    else:
        assert not error_count
        logger.info(">>> There were no errors.")

    return bool(has_errored)


if __name__ == '__main__':
    error = main()
    sys.exit(error)
