#!/usr/bin/env python3

# A simple script to help delete a failing primitive annotation. The script can be called in two ways:
#
#  - with primitive ID as reported by the validation script (e.g., d3m.primitives.dsbox.MultiTableFeaturization/0.1.3)
#  - with path to failing primitive annotation file (e.g., ./primitives/ISI/d3m.primitives.dsbox.RandomProjectionTimeSeriesFeaturization/0.1.3/primitive.json)

import argparse
import glob
import os
import os.path
import shutil

parser = argparse.ArgumentParser(description="Delete failing primitives from repository.")
parser.add_argument('primitive_names', metavar='primitive', nargs='+', help="primitive name to delete")
arguments = parser.parse_args()

for primitive_name in arguments.primitive_names:
    if os.path.exists(primitive_name):
        segments = primitive_name.split('/')
        python_path, version, filename = [segments[i] for i in [-3, -2, -1]]
        if filename != 'primitive.json':
            raise ValueError("Not a path to \"primitive.json\" file: " + primitive_name)
    else:
        python_path, version = primitive_name.split('/')

    for globbed_file_path in glob.glob('primitives/*/{python_path}/{version}'.format(python_path=python_path, version=version)):
        shutil.rmtree(globbed_file_path, ignore_errors=True)
