{
    "algorithm_types": [
        "NUMERICAL_METHOD"
    ],
    "description": "This primitive converts longitude/latitude coordinates into geographic location names.",
    "digest": "1bbf5538b43929219dadd2619676211dfca837ebae0e0123a0a24c274afad30d",
    "id": "f6e4880b-98c7-32f0-b687-a4b1d74c8f99",
    "installation": [
        {
            "package": "cython",
            "type": "PIP",
            "version": "0.29.24"
        },
        {
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/contrib/kungfuai-primitives.git@49acb225bb6994d3dfaffdf3b7761395423680a4#egg=kf-d3m-primitives",
            "type": "PIP"
        },
        {
            "package": "openjdk-8-jdk-headless",
            "type": "UBUNTU",
            "version": "8u252-b09-1~18.04"
        },
        {
            "file_digest": "d7e3d5c6ae795b5f53d31faa3a9af63a9691070782fa962dfcd0edf13e8f1eab",
            "file_uri": "http://public.datadrivendiscovery.org/photon.tar.gz",
            "key": "photon-db-latest",
            "type": "TGZ"
        }
    ],
    "keywords": [
        "Reverse Geocoder"
    ],
    "name": "Goat_reverse",
    "original_python_path": "kf_d3m_primitives.data_preprocessing.geocoding_reverse.goat_reverse.GoatReversePrimitive",
    "primitive_code": {
        "arguments": {
            "hyperparams": {
                "kind": "RUNTIME",
                "type": "kf_d3m_primitives.data_preprocessing.geocoding_reverse.goat_reverse.Hyperparams"
            },
            "inputs": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "iterations": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, int]"
            },
            "params": {
                "kind": "RUNTIME",
                "type": "NoneType"
            },
            "produce_methods": {
                "kind": "RUNTIME",
                "type": "typing.Sequence[str]"
            },
            "random_seed": {
                "default": 0,
                "kind": "RUNTIME",
                "type": "int"
            },
            "timeout": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, float]"
            },
            "volumes": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, typing.Dict[str, str]]"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "class_methods": {},
        "class_type_arguments": {
            "Hyperparams": "kf_d3m_primitives.data_preprocessing.geocoding_reverse.goat_reverse.Hyperparams",
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "NoneType"
        },
        "hyperparams": {
            "cache_size": {
                "default": 2000,
                "description": "LRU cache size",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 9223372036854775807,
                "upper_inclusive": false
            },
            "geocoding_resolution": {
                "default": "country",
                "description": "granularity of geocoding resolution",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "str",
                "type": "d3m.metadata.hyperparams.Enumeration",
                "values": [
                    "city",
                    "country",
                    "state",
                    "postcode"
                ]
            },
            "rampup_timeout": {
                "default": 100,
                "description": "timeout, how much time to give elastic search database to startup,             may vary based on infrastructure",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 9223372036854775807,
                "upper_inclusive": false
            }
        },
        "instance_attributes": {
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "temporary_directory": "typing.Union[NoneType, str]",
            "volumes": "typing.Dict[str, str]"
        },
        "instance_methods": {
            "__init__": {
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "volumes"
                ],
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "fit": {
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "description": "A noop.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]"
            },
            "fit_multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "get_params": {
                "arguments": [],
                "description": "A noop.\n\nReturns\n-------\nAn instance of parameters.",
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "produce": {
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "Accept a set of longitude/latitude pairs, processes it and returns a set of corresponding\ngeographic location names\n\nParameters\n----------\ninputs: D3M dataframe containing 2 coordinate float values, i.e., [longitude,latitude]\n         representing each geographic location of interest - a pair of values\n         per location/row in the specified target column\n\nReturns\n-------\nOutputs: D3M dataframe containing one location per longitude/latitude pair appended as new columns\n    (if reverse geocoding possible, otherwise NaNs)",
                "inputs_across_samples": [],
                "kind": "PRODUCE",
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false
            },
            "set_params": {
                "arguments": [
                    "params"
                ],
                "description": "A noop.\n\nParameters\n----------\nparams:\n    An instance of parameters.",
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "set_training_data": {
                "arguments": [],
                "description": "A noop.\n\nParameters\n----------",
                "kind": "OTHER",
                "returns": "NoneType"
            }
        },
        "interfaces": [
            "transformer.TransformerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "interfaces_version": "2021.11.25.dev0"
    },
    "primitive_family": "DATA_CLEANING",
    "python_path": "d3m.primitives.data_cleaning.geocoding.Goat_reverse",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "source": {
        "contact": "mailto:cbethune@uncharted.software",
        "name": "Distil",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/contrib/kungfuai-primitives"
        ]
    },
    "structural_type": "kf_d3m_primitives.data_preprocessing.geocoding_reverse.goat_reverse.GoatReversePrimitive",
    "version": "1.0.8"
}
