{
    "algorithm_types": [
        "T_DISTRIBUTED_STOCHASTIC_NEIGHBOR_EMBEDDING"
    ],
    "description": "This primitive applies scikit-learn's T-distributed stochastic neighbour embedding algorithm.",
    "digest": "1a9ab8a7b43b7c0e9c06d9aa0eb921ad7a24bb4a40efce96e576fc19ede2d82b",
    "id": "15586787-80d5-423e-b232-b61f55a117ce",
    "installation": [
        {
            "package": "cython",
            "type": "PIP",
            "version": "0.29.24"
        },
        {
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/contrib/kungfuai-primitives.git@49acb225bb6994d3dfaffdf3b7761395423680a4#egg=kf-d3m-primitives",
            "type": "PIP"
        }
    ],
    "keywords": [
        "Dimensionality Reduction"
    ],
    "name": "tsne",
    "original_python_path": "kf_d3m_primitives.dimensionality_reduction.tsne.Tsne.TsnePrimitive",
    "primitive_code": {
        "arguments": {
            "hyperparams": {
                "kind": "RUNTIME",
                "type": "kf_d3m_primitives.dimensionality_reduction.tsne.Tsne.Hyperparams"
            },
            "inputs": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "iterations": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, int]"
            },
            "params": {
                "kind": "RUNTIME",
                "type": "NoneType"
            },
            "produce_methods": {
                "kind": "RUNTIME",
                "type": "typing.Sequence[str]"
            },
            "random_seed": {
                "default": 0,
                "kind": "RUNTIME",
                "type": "int"
            },
            "timeout": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, float]"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "class_methods": {},
        "class_type_arguments": {
            "Hyperparams": "kf_d3m_primitives.dimensionality_reduction.tsne.Tsne.Hyperparams",
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "NoneType"
        },
        "hyperparams": {
            "n_components": {
                "default": 2,
                "description": "dimension of the embedded space",
                "lower": 1,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "structural_type": "int",
                "type": "d3m.metadata.hyperparams.UniformInt",
                "upper": 3,
                "upper_inclusive": true
            }
        },
        "instance_attributes": {
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "temporary_directory": "typing.Union[NoneType, str]",
            "volumes": "typing.Dict[str, str]"
        },
        "instance_methods": {
            "__init__": {
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "fit": {
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "description": "A noop.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]"
            },
            "fit_multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "get_params": {
                "arguments": [],
                "description": "A noop.\n\nReturns\n-------\nAn instance of parameters.",
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "produce": {
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\ninputs : D3M dataframe with attached metadata for semi-supervised or unsupervised data\n\nReturns\n-------\nOutputs:\n    D3M dataframe with t-SNE dimensions and D3M indices",
                "inputs_across_samples": [],
                "kind": "PRODUCE",
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false
            },
            "set_params": {
                "arguments": [
                    "params"
                ],
                "description": "A noop.\n\nParameters\n----------\nparams:\n    An instance of parameters.",
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "set_training_data": {
                "arguments": [],
                "description": "A noop.\n\nParameters\n----------",
                "kind": "OTHER",
                "returns": "NoneType"
            }
        },
        "interfaces": [
            "transformer.TransformerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "interfaces_version": "2021.11.25.dev0"
    },
    "primitive_family": "DIMENSIONALITY_REDUCTION",
    "python_path": "d3m.primitives.dimensionality_reduction.t_distributed_stochastic_neighbor_embedding.Tsne",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "source": {
        "contact": "mailto:cbethune@uncharted.software",
        "name": "Distil",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/contrib/kungfuai-primitives"
        ]
    },
    "structural_type": "kf_d3m_primitives.dimensionality_reduction.tsne.Tsne.TsnePrimitive",
    "version": "1.0.0"
}
